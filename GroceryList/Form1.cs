﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroceryList
{
    public partial class Form1 : Form
    {
        private GroceryItem temp;
        private List<GroceryItem> GroceryList = new List<GroceryItem>();
        public Form1()
        {
            InitializeComponent();
        }

        //ADDS NEW DATA IN THE LIST
        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            Boolean have = false, need = false;
            if(radioButton1.Checked)
            {
                have = true;
            }
            GroceryItem new_item = new GroceryItem(name, have, need);
            GroceryList.Add(new_item);
            show_data();
            clear_fields();
        }

        //CLEARS THE INPUT FIELDS
        private void clear_fields()
        {
            textBox1.Text = "";
            radioButton1.Checked = radioButton2.Checked = false;
        }

        //LOADS DATA INTO THE DATAGRIDVIEW
        private void show_data()
        {
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            foreach(GroceryItem i in GroceryList)
            {
                DataGridViewRow new_row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                new_row.Cells[0].Value = i.Name;
                if(i.Have)
                {
                    dataGridView1.Rows.Add(new_row);
                }
                else
                {
                    dataGridView2.Rows.Add(new_row);
                }
            }
        }

        //LOADS DATA INTO TEXT BOX AND RADIO BUTTONS WHEN ONE OF THE CELLS IS CLICKED
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string name;
            if(sender == dataGridView1)
            {
                name = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
            else
            {
                name = dataGridView2.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            }
            temp = GroceryList.Where(x => x.Name.Equals(name)).First();
            textBox1.Text = temp.Name;
            if(temp.Have)
            {
                radioButton1.Checked = true;
            }
            else
            {
                radioButton2.Checked = true;
            }
        }

        //UPDATES EXISTING ITEM ON THE LIST
        private void button2_Click(object sender, EventArgs e)
        {
            int index = GroceryList.FindIndex(x => x.Name.Equals(temp.Name));
            GroceryList[index].Name = textBox1.Text;
            if(radioButton1.Checked)
            {
                GroceryList[index].Have = true;
                GroceryList[index].Need = false;
            }
            else
            {
                GroceryList[index].Need = true;
                GroceryList[index].Have = false;
            }
            show_data();
            clear_fields();
        }

        //DELETES AN ITEM FROM THE LIST
        private void button3_Click(object sender, EventArgs e)
        {
            int index = GroceryList.IndexOf(temp);
            GroceryList.RemoveAt(index);
            show_data();
            clear_fields();
        }

        //SAVES THE ITEMS OF THE LIST IN A TEXT FILE LOCATED INSIDE THE BIN/DEBUG FOLDER
        private void button4_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("grocery_list.txt");
            sw.Write("HAVE : ");
            foreach (GroceryItem i in GroceryList)
            {
                if(i.Have)
                {
                    sw.Write(" " + i.Name);
                }
            }
            sw.WriteLine();
            sw.Write("NEED : ");
            foreach (GroceryItem i in GroceryList)
            {
                if (i.Need)
                {
                    sw.Write(" " + i.Name);
                }
            }
            sw.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }

    class GroceryItem
    {
        string name;
        Boolean have, need;


        //GETS AND SETS
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public bool Have
        {
            get
            {
                return have;
            }

            set
            {
                have = value;
            }
        }

        public bool Need
        {
            get
            {
                return need;
            }

            set
            {
                need = value;
            }
        }

        //CONSTRUCTOR OF VALUES
        public GroceryItem(string name, bool have, bool need)
        {
            this.Name = name;
            this.Have = have;
            this.Need = need;
        }
    }
}
