﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContactList
{
    public partial class Form1 : Form
    {
        //LIST FOR MAINTAINING CONTACTS
        static List<Contact> contact_list = new List<Contact>();
        static Contact temp;
        public Form1()
        {
            InitializeComponent();
        }


        //ADDING A NEW CONTACT
        private void btn_add_Click(object sender, EventArgs e)
        {
            string first_name = tb_first_name.Text;
            string last_name = tb_last_name.Text;
            string phone = tb_phone.Text;
            string email = tb_email.Text;
            string icon = "small";
            if(rb_large.Checked)
            {
                icon = "large";
            }

            //INPUT FIELD VALIDATIONS DEFORE ADDING A NEW CONTACT
            if(first_name.Any(char.IsDigit) || last_name.Any(char.IsDigit))
            {
                MessageBox.Show("Name cannot contain a number");
            }
            else if(phone.Any(char.IsLetter))
            {
                MessageBox.Show("Phone cannot contain letters.");
            }
            else
            {
                try
                {
                    MailAddress check_mail = new MailAddress(email);
                    Contact new_contact = new Contact(first_name, last_name, phone, email, icon);
                    contact_list.Add(new_contact);
                }
                catch(Exception)
                {
                    MessageBox.Show("Email format is not correct.");
                }
            }
            show_data();
            clear_fields();
        }


        //FUNCTION FOR CLEARING INPUT FIELDS
        private void clear_fields()
        {
            tb_first_name.Text = tb_last_name.Text = tb_phone.Text = tb_email.Text = "";
            rb_small.Checked = rb_large.Checked = false;
        }

        //SHOWS DATA IN THE DATAGRIDVIEW
        private void show_data()
        {
            dataGridView1.Rows.Clear();
            foreach(Contact i in contact_list)
            {
                DataGridViewRow new_row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
                if(i.Icon == "small")
                {
                    new_row.Cells[0].Value = Properties.Resources.small;
                }
                else
                {
                    new_row.Cells[0].Value = Properties.Resources.large;
                }
                new_row.Cells[1].Value = i.First_name;
                new_row.Cells[2].Value = i.Last_name;
                new_row.Cells[3].Value = i.Phone_number;
                new_row.Cells[4].Value = i.Email;
                dataGridView1.Rows.Add(new_row);
            }
        }


        //UPDATING A CONTACT
        private void btn_update_Click(object sender, EventArgs e)
        {
            string first_name = tb_first_name.Text;
            string last_name = tb_last_name.Text;
            string phone = tb_phone.Text;
            string email = tb_email.Text;
            string icon = "small";
            if (rb_large.Checked)
            {
                icon = "large";
            }
            Contact update = new Contact(first_name, last_name, phone, email, icon);
            int index = contact_list.FindIndex(x => x.Email.Equals(temp.Email));
            contact_list[index] = update;
            show_data();
            clear_fields();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            contact_list.RemoveAt(contact_list.FindIndex(x => x.Email.Equals(tb_email.Text)));
            show_data();
            clear_fields();
        }


        //SAVES CONTACT LIST IN A .txt FILE
        private void btn_save_Click(object sender, EventArgs e)
        {
            StreamWriter sw = new StreamWriter("contact_list.txt");
            foreach(Contact i in contact_list)
            {
                sw.WriteLine(i.First_name + "\t" + i.Last_name + "\t" + i.Phone_number + "\t" + i.Email);
            }
            sw.Close();
        }


        //LOADS DATA FROM THE DAGRIDVIEW INTO THE INPUT FIELDS ON A CELL CLICK
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            temp = new Contact("", "", "", "", "");
            temp.Email = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            temp = contact_list.Where(x => x.Email.Equals(temp.Email)).First();
            tb_first_name.Text = temp.First_name;
            tb_last_name.Text = temp.Last_name;
            tb_phone.Text = temp.Phone_number;
            tb_email.Text = temp.Email;
            if(temp.Icon == "small")
            {
                rb_small.Checked = true;
            }
            else
            {
                rb_large.Checked = true;
            }
        }
    }
}
