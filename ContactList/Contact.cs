﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactList
{
    //CREATES CLASS THAT WILL CARRY PARAMETERS
    class Contact
    {
        private string first_name, last_name, phone_number, email, icon;

        public Contact(string first_name, string last_name, string phone_number, string email, string icon)
        {
            this.First_name = first_name;
            this.Last_name = last_name;
            this.Phone_number = phone_number;
            this.Email = email;
            this.icon = icon;
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string First_name
        {
            get
            {
                return first_name;
            }

            set
            {
                first_name = value;
            }
        }

        public string Icon
        {
            get
            {
                return icon;
            }

            set
            {
                icon = value;
            }
        }

        public string Last_name
        {
            get
            {
                return last_name;
            }

            set
            {
                last_name = value;
            }
        }

        public string Phone_number
        {
            get
            {
                return phone_number;
            }

            set
            {
                phone_number = value;
            }
        }
    }
}
