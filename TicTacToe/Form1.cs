﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class board : Form
    {   
        //CREATES BOARD, CREATES STRING 'PLAYER', INT COUNT AND ASSIGN TAGS TO EVERY BOX.
        private int count = 0;
        private string player1;
        public board()
        {
            MaximizeBox = false;
            InitializeComponent();
            A1.AllowDrop = A2.AllowDrop = A3.AllowDrop = true;
            B1.AllowDrop = B2.AllowDrop = B3.AllowDrop = true;
            C1.AllowDrop = C2.AllowDrop = C3.AllowDrop = true;
            A1.Tag = "A1";
            A2.Tag = "A2";
            A3.Tag = "A3";
            B1.Tag = "B1";
            B2.Tag = "B2";
            B3.Tag = "B3";
            C1.Tag = "C1";
            C2.Tag = "C2";
            C3.Tag = "C3";
        }
        //ALLOWS INTERACTION WITH MOUSE
        private void yes_MouseDown(object sender, MouseEventArgs e)
        {
            DoDragDrop(yes.Image, DragDropEffects.Move);
        }

        private void A1_DragDrop(object sender, DragEventArgs e)
        {   
            //METHOD CALLING THE CHECK WINNER AND ALSO COUNT VARIABLE TO TRACK NUMBER OF MOVES BY PLAYER
            string player;
            if(yes.Enabled == true)
            {
                player = "yes";
            }
            else
            {
                player = "no";
            }
            //CHECKS FOR THE CORRECT IMAGE AND CALCULATES IF THERE IS A WINNER OR A TIE
            var bmp = (Bitmap)e.Data.GetData(DataFormats.Bitmap);
            ((PictureBox)sender).Image = bmp;
            ((PictureBox)sender).AllowDrop = false;
            ((PictureBox)sender).Tag = player;
            Image winner = check_winnder();
            if (winner != null)
            {
                new Winner(winner, " is the winner", this).Show();
            }
            if(count == 9)
            {
                new Winner(null, "It is a tie.", this).Show();
            }
            count++;
            if(yes.Enabled == true)
            {
                no.Enabled = true;
                yes.Enabled = false;
            }
            else
            {
                no.Enabled = false;
                yes.Enabled = true;
            }
        }

        private void A1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Bitmap))
                e.Effect = DragDropEffects.Move;
        }

        private void no_MouseDown(object sender, MouseEventArgs e)
        {
            DoDragDrop(no.Image, DragDropEffects.Move);
        }

        private Image check_winnder()
        {   
            //CHECKS IF IMAGES ARE ALIGNED VERTICALLY OR HORIZONTALLY TO DECLARE A WINNER OR LOSER
            if ((A1.Tag == A2.Tag && A2.Tag == A3.Tag) || 
                (A1.Tag == B2.Tag && B2.Tag == C3.Tag) ||
                (A1.Tag == B1.Tag && B1.Tag == C1.Tag))
            {
                return A1.Image;
            }
            else if (B1.Tag == B2.Tag && B2.Tag == B3.Tag)
            {
                return B1.Image;
            }
            else if ((C1.Tag == C2.Tag && C2.Tag == C3.Tag) ||
                (C1.Tag == B2.Tag && B2.Tag == A3.Tag))
            {
                return C1.Image;
            }
            else if(A2.Tag == B2.Tag && B2.Tag == C2.Tag)
            {
                return A2.Image;
            }
            else if((A3.Tag == B3.Tag && B3.Tag == C3.Tag))
            {
                return A3.Image;
            }
            return null;
        }
        //SETS UP PLAYER BASED ON SELECTION
        private void rb_yes_Click(object sender, EventArgs e)
        {
            player1 = "yes";
            rb_yes.Checked = true;
            rb_yes.Enabled = false;  //PREVENTS USER FROM SELECTING PLAYER TWICE
            rb_no.Enabled = false;
            no.Enabled = false;
        }
        //SETS UP PLAYER BASED ON SELECTION
        private void rb_no_Click(object sender, EventArgs e)
        {
            player1 = "no";
            rb_no.Checked = true;
            rb_yes.Enabled = false;  //PREVENTS USER FROM SELECTING PLAYER TWICE
            rb_no.Enabled = false;
            yes.Enabled = false;
        }
    }
}
