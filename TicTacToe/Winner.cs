﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{   //INHERITS PARAMETERS FROM THE FORM IN ORDER TO SET UP THE GAME
    public partial class Winner : Form
    {
        private Form parent;
        public Winner(Image winner, string msg, Form parent)
        {
            MaximizeBox = false;
            InitializeComponent();
            pictureBox1.Image = winner;
            label1.Text = msg;
            this.parent = parent;
        }
        //THIS WILL TRIGGER WHEN THE USER PRESS OK
        private void ok_Click(object sender, EventArgs e)
        {
            Close();
            parent.Close();
        }
    }
}
