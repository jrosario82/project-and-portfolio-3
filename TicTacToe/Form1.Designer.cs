﻿namespace TicTacToe
{
    partial class board
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(board));
            this.yes = new System.Windows.Forms.PictureBox();
            this.no = new System.Windows.Forms.PictureBox();
            this.A3 = new System.Windows.Forms.PictureBox();
            this.A2 = new System.Windows.Forms.PictureBox();
            this.C2 = new System.Windows.Forms.PictureBox();
            this.C1 = new System.Windows.Forms.PictureBox();
            this.A1 = new System.Windows.Forms.PictureBox();
            this.B1 = new System.Windows.Forms.PictureBox();
            this.B2 = new System.Windows.Forms.PictureBox();
            this.B3 = new System.Windows.Forms.PictureBox();
            this.C3 = new System.Windows.Forms.PictureBox();
            this.rb_yes = new System.Windows.Forms.RadioButton();
            this.rb_no = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.yes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.no)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).BeginInit();
            this.SuspendLayout();
            // 
            // yes
            // 
            this.yes.Image = ((System.Drawing.Image)(resources.GetObject("yes.Image")));
            this.yes.ImageLocation = "";
            this.yes.Location = new System.Drawing.Point(65, 237);
            this.yes.Name = "yes";
            this.yes.Size = new System.Drawing.Size(35, 33);
            this.yes.TabIndex = 0;
            this.yes.TabStop = false;
            this.yes.MouseDown += new System.Windows.Forms.MouseEventHandler(this.yes_MouseDown);
            // 
            // no
            // 
            this.no.Image = ((System.Drawing.Image)(resources.GetObject("no.Image")));
            this.no.Location = new System.Drawing.Point(264, 237);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(35, 33);
            this.no.TabIndex = 1;
            this.no.TabStop = false;
            this.no.MouseDown += new System.Windows.Forms.MouseEventHandler(this.no_MouseDown);
            // 
            // A3
            // 
            this.A3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A3.Location = new System.Drawing.Point(264, 12);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(35, 33);
            this.A3.TabIndex = 2;
            this.A3.TabStop = false;
            this.A3.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.A3.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // A2
            // 
            this.A2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A2.Location = new System.Drawing.Point(162, 12);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(35, 33);
            this.A2.TabIndex = 3;
            this.A2.TabStop = false;
            this.A2.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.A2.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // C2
            // 
            this.C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C2.Location = new System.Drawing.Point(162, 166);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(35, 33);
            this.C2.TabIndex = 4;
            this.C2.TabStop = false;
            this.C2.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.C2.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // C1
            // 
            this.C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C1.Location = new System.Drawing.Point(65, 166);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(35, 33);
            this.C1.TabIndex = 5;
            this.C1.TabStop = false;
            this.C1.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.C1.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // A1
            // 
            this.A1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.A1.Location = new System.Drawing.Point(65, 12);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(35, 33);
            this.A1.TabIndex = 6;
            this.A1.TabStop = false;
            this.A1.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.A1.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // B1
            // 
            this.B1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B1.Location = new System.Drawing.Point(65, 91);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(35, 33);
            this.B1.TabIndex = 7;
            this.B1.TabStop = false;
            this.B1.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.B1.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // B2
            // 
            this.B2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B2.Location = new System.Drawing.Point(162, 91);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(35, 33);
            this.B2.TabIndex = 8;
            this.B2.TabStop = false;
            this.B2.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.B2.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // B3
            // 
            this.B3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.B3.Location = new System.Drawing.Point(264, 91);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(35, 33);
            this.B3.TabIndex = 9;
            this.B3.TabStop = false;
            this.B3.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.B3.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // C3
            // 
            this.C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.C3.Location = new System.Drawing.Point(264, 166);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(35, 33);
            this.C3.TabIndex = 10;
            this.C3.TabStop = false;
            this.C3.DragDrop += new System.Windows.Forms.DragEventHandler(this.A1_DragDrop);
            this.C3.DragEnter += new System.Windows.Forms.DragEventHandler(this.A1_DragEnter);
            // 
            // rb_yes
            // 
            this.rb_yes.AutoCheck = false;
            this.rb_yes.AutoSize = true;
            this.rb_yes.Location = new System.Drawing.Point(45, 237);
            this.rb_yes.Name = "rb_yes";
            this.rb_yes.Size = new System.Drawing.Size(14, 13);
            this.rb_yes.TabIndex = 11;
            this.rb_yes.UseVisualStyleBackColor = true;
            this.rb_yes.Click += new System.EventHandler(this.rb_yes_Click);
            // 
            // rb_no
            // 
            this.rb_no.AutoCheck = false;
            this.rb_no.AutoSize = true;
            this.rb_no.Location = new System.Drawing.Point(244, 237);
            this.rb_no.Name = "rb_no";
            this.rb_no.Size = new System.Drawing.Size(14, 13);
            this.rb_no.TabIndex = 12;
            this.rb_no.UseVisualStyleBackColor = true;
            this.rb_no.Click += new System.EventHandler(this.rb_no_Click);
            // 
            // board
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 282);
            this.Controls.Add(this.rb_no);
            this.Controls.Add(this.rb_yes);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.yes);
            this.Controls.Add(this.no);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "board";
            this.Text = "Tic Tac Toe";
            ((System.ComponentModel.ISupportInitialize)(this.yes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.no)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox yes;
        private System.Windows.Forms.PictureBox no;
        private System.Windows.Forms.PictureBox A3;
        private System.Windows.Forms.PictureBox A2;
        private System.Windows.Forms.PictureBox C2;
        private System.Windows.Forms.PictureBox C1;
        private System.Windows.Forms.PictureBox A1;
        private System.Windows.Forms.PictureBox B1;
        private System.Windows.Forms.PictureBox B2;
        private System.Windows.Forms.PictureBox B3;
        private System.Windows.Forms.PictureBox C3;
        private System.Windows.Forms.RadioButton rb_yes;
        private System.Windows.Forms.RadioButton rb_no;
    }
}

